import os
import pathlib
import werkzeug

from flask import Flask
from flask_restful import Api, Resource, reqparse


UPLOAD_PATH = os.path.dirname(__file__) + "/uploaded"


class FileUpload(Resource):
    def __init__(self):
        self._parser = reqparse.RequestParser()
        self._parser.add_argument(
            "file", type=werkzeug.datastructures.FileStorage, location="files"
        )

    def post(self):
        data = self._parser.parse_args()
        print(data)
        file = data["file"]
        if file is None:
            return {"message": "No file found"}, 400

        file.save(os.path.join(UPLOAD_PATH, file.filename))
        return {"message": "File uploaded"}, 200


def create_app():
    app = Flask(__name__)
    api = Api(app)

    api.add_resource(FileUpload, "/upload")
    pathlib.Path(UPLOAD_PATH).mkdir(exist_ok=True)

    return app
