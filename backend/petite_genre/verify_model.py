import sys
import os


PACKAGE_PARENT = ".."
SCRIPT_DIR = os.path.dirname(
    os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)))
)
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))


from common import Prediction
from genre_recognition.genre_recognizer import GenreRecognizer
from genre_recognition.common import GENRES
from genre_recognition import MODEL_PATH
from prediction_observer import PredictionObserver


class Verifier(object):
    def __init__(self, path_to_dataset, model_path):
        self._genre_recognizer = GenreRecognizer(model_path)
        self._path_to_dataset = path_to_dataset
        self._good_predictions_count = 0
        self._processed_tracks_count = 0
        self._prediction_observers: list[PredictionObserver] = []

    def attach_observer(self, observer: PredictionObserver):
        self._prediction_observers.append(observer)

    def verify_model(self):
        for genre in GENRES:
            self._validate_genre(genre)
        return self._good_predictions_count / self._processed_tracks_count

    def _notify_observers(self, prediction: Prediction):
        for observer in self._prediction_observers:
            observer.notify(prediction)

    def _validate_genre(self, genre):
        genre_path = os.path.join(self._path_to_dataset, genre)
        for file in os.listdir(genre_path):
            if file.endswith(".wav"):
                prediction = self._get_prediction_for_track(
                    os.path.join(genre_path, file)
                )

                prediction.genre = genre
                self._notify_observers(prediction)

                if prediction.predicted_genre == genre:
                    self._good_predictions_count += 1
                self._processed_tracks_count += 1

    def _get_prediction_for_track(self, track_path):
        prediction = Prediction()
        prediction.file = track_path

        predictions, duration, before_softmax = self._genre_recognizer.recognize(
            track_path
        )
        distribution = self._genre_recognizer.get_genre_distribution_over_time(
            predictions, duration
        )

        mean_distribution = {genre: 0.0 for genre in GENRES}
        for _, distrib in distribution:
            for genre in GENRES:
                mean_distribution[genre] += distrib[genre]

        for genre in GENRES:
            mean_distribution[genre] /= len(distribution)

        prediction.before_softmax = before_softmax
        prediction.predicted_genre = max(mean_distribution, key=mean_distribution.get)
        return prediction


if __name__ == "__main__":
    dataset_path = sys.argv[1]

    with PredictionObserver("predictions_output") as observer:
        verifier = Verifier(dataset_path, MODEL_PATH)
        verifier.attach_observer(observer)
        good_predictions = verifier.verify_model()
        print(f"Good predictions: {good_predictions}")
