import numpy as np

from common import Prediction


class PredictionObserver:
    def __init__(self, file_name: str):
        self._file_name = file_name
        self._output_file = None

    def __enter__(self):
        self._output_file = open(self._file_name, "w+")
        return self

    def __exit__(self, *args):
        self._output_file.close()

    def notify(self, prediction: Prediction):
        self._output_file.write("Predicting file " + prediction.file + "\n")
        self._output_file.write("Predicted genre: " + prediction.predicted_genre + "\n")
        self._output_file.write("Actual: " + prediction.genre + "\n")
        self._output_file.write(
            "Index with biggest val: "
            + str(np.argmax(prediction.before_softmax))
            + "\n\n"
        )
