import numpy as np

from dataclasses import dataclass


@dataclass
class Prediction:
    file: str = ""
    genre: str = ""
    predicted_genre: str = ""
    before_softmax: np.ndarray = None
