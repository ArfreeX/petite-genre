import numpy as np
from genre_recognition.common import load_track, get_layer_output_function, GENRES
from tensorflow.keras.models import load_model


class GenreRecognizer:
    def __init__(self, model_path):
        model = load_model(model_path)
        self.pred_fun = get_layer_output_function(model, "output_realtime")
        self.before_softmax_fun = get_layer_output_function(model, "output_merged")
        print("Loaded model.")

    def recognize(self, track_path):
        print("Loading song", track_path)
        (features, duration) = load_track(track_path)
        features = np.reshape(features, (1,) + features.shape)
        return self.pred_fun(features), duration, self.before_softmax_fun(features)

    def get_genre_distribution_over_time(self, predictions, duration):
        """
        Turns the matrix of predictions given by a model into a dict mapping
        time in the song to a music genre distribution.
        """
        predictions = np.reshape(predictions, predictions.shape[1:])
        n_steps = predictions.shape[0]
        delta_t = duration / n_steps

        def get_genre_distribution(step):
            return {
                genre_name: float(predictions[step, genre_index])
                for (genre_index, genre_name) in enumerate(GENRES)
            }

        return [
            ((step + 1) * delta_t, get_genre_distribution(step))
            for step in range(n_steps)
        ]
