import React, {useCallback} from 'react'
import {useDropzone} from 'react-dropzone'
import {api} from '../api'

function DropZone() {
  const onDrop = useCallback(acceptedFiles => {
      acceptedFiles.forEach(async (file) => {
        try {
          await api.uploadFile(file);
        } catch (error) {
          console.log(error);
        }
      });
  }, []);
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      {
        isDragActive ?
          <p>Drop the files here ...</p> :
          <p>Drag 'n' drop some files here, or click to select files</p>
      }
    </div>
  );
}

const FileUploadView = () => {
    return (
        <div>
            <DropZone />
        </div>
    );
};

export default FileUploadView;
