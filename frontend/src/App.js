import FileUploadView from './components/FileUploadView'

function App() {
  return (
    <div className="App">
        <FileUploadView />
    </div>
  );
}

export default App;
