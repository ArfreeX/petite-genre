import axios from 'axios'

export const api = {
    uploadFile: async (file) => {
        const formData = new FormData();
        console.log(file);
        console.log('xxx');
        formData.append('file', file);
        console.log(formData);
        return await axios.post('upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        });
    }
};
